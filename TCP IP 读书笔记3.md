# **IP相当于OSI参考模型的第3层 主机与节点**
- [节点]是主机和[路由器]的统称
- 既配有IP地址又具有路由控制能力的设备叫做“[路由器]”，跟主机有所区别。
- 准确地说，[主机]的定义应该是指“配置有[IP地址]，但是不进行[路由控制]的设备”。
- [路由控制]是指中转[分组数据包]。
- [主机]英文叫做Host。
- [路由控制]英文叫做Routing。
- [路由器]英文叫做Router。
- [节点]英文叫做Nodes。
- [分组数据包]英文叫做Packets。